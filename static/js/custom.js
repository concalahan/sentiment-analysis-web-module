$().ready(function () {
    // Hiding compare section
    $(".compare-section").hide();

    $("#clear-demo-vsa").on("click", function() {
        // For analyze area
        $("#demo-content").text("");
        $("#demo-content").val("");

        // For compare area
        $("#product-compare-area").html(
            `<input class="form-control mt-1 product-compare" type="text" placeholder="Ex: iPhone XS 64GB">`
        );
    });

    $("#change-btn").on("click", function() {
        $(".analyze-section").toggle();
        $(".compare-section").toggle();
        if ($(this).text() == "Chuyển sang so sánh") {
            $(this).text("Chuyển sang đánh giá");
        } else {
            $(this).text("Chuyển sang so sánh");
        }
    });

    $("#add-compare-product").on("click", function() {
        $("#product-compare-area").append(
            `<input class="form-control mt-1 product-compare" type="text" placeholder="Ex: iPhone XS 64GB">`
        );
    });

    $("#submit-demo-vsa").on("click", function() {
        // Clear content
        $(".display-demo").text("");

        if ($("#change-btn").text() == "Chuyển sang so sánh") {
            var query = $("#demo-content").val();

            $.ajax({
                url: "/analyze-text",
                dataType: "json",
                type: "POST",
                contentType: 'application/json',
                data: JSON.stringify(
                    {"q" : query}
                ),
                processData: false,
                success(result) {
                    $(".display-demo").text("");
                    console.log(result);
                    data = result.data;
                    
                    if (data != [] && data.length != 0) {
                        console.log("hahaha " + data);
                        for (let i = 0;i < data.length; i++) {
                    
                            let sentence = data[i].sentence;
                            let entity = data[i].entity;
                            let keywords = data[i].keywords;
                            let score = data[i].score;
                            let evaluation = data[i].sentiment;
                            $(".display-demo").append(demoAnalyzeContent(sentence, entity, keywords, score, evaluation));
                        } 
                    }
                    else {
                        $(".display-demo").text("Không phát hiện được bất kì entity thuộc lĩnh vực công nghệ nào.");
                    }
                    
                },
                error(error) {
                    console.log(error);
                }
            });
        } 
        else {
            var productModel = $("#product-model").val();
            var productCompare = $(".product-compare");
            var products = [];

            for(let i = 0; i < productCompare.length; i++){
                let product = {
                    "id" : "1",
                    "name" : $(productCompare[i]).val()
                }
                products.push(product);
            }

            $.ajax({
                url: "/find-similarity",
                dataType: "json",
                type: "POST",
                contentType: 'application/json',
                data: JSON.stringify(
                    {
                        "productModel" : productModel,
                        "products" : products,
                    }
                ),
                processData: false,
                success(data) {
                    $(".display-demo").text("");
                    console.log(data);
                    
                    if (data != [] && data.length != 0) {
                        for (let i = 0;i < data.length; i++) {
                            
                            let product = data[i].name;
                            let score = data[i].score;
                            $(".display-demo").append(demoCompareContent(product, score));
                        }

                        
                    }
                    else {
                        $(".display-demo").text("");
                    }
                },
                error(error) {
                    console.log(error);
                }
            });
        }
    });

});

function demoAnalyzeContent(sentence, entity, keywords, score, evaluation) {
    score = (score*100) + " %";
    content = 
    `
    <div class="ml-3" >
        <div class="row">
            <label style="color: #33cc33">Sentence: </label><span>&nbsp;&nbsp;` + sentence + `</span>
        </div>
        <div class="row mb-2">
            <label style="color: #ff1a1a">Entity: </label> <span>&nbsp;` + entity + `</span>
            <label class="ml-2" style="color: #ff4d4d">Keywords:</label><span>&nbsp;` + keywords + `</span>
            <label class="ml-2" style="color: #ff8080">Evaluation: </label><span>&nbsp;` + evaluation + `</span>
            <label class="ml-2" style="color: #ffb3b3">Score: </label><span>&nbsp;` + score + `</span>
        </div>
    </div>
    `;

    return content;
}

function demoCompareContent(product, score) {
    
    content = 
    `
    <div class="ml-3" >
        <div class="row">
            <label style="color: #33cc33">Product: </label><span>&nbsp;&nbsp;` + product + `</span>
        </div>
        <div class="row mb-2">
            <label style="color: #ff1a1a">Matching rate: </label> <span>&nbsp;` + score + `</span>
        </div>
    </div>
    `;

    return content;
}