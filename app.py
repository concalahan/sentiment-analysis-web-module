# -*- coding: utf-8 -*-
import sys
sys.path.append('./crawling-article/')
sys.path.append('./models/')
sys.path.append('./services/')

import os
import json
import pickle
import numpy as np
import operator

from collections import OrderedDict
from tempfile import gettempdir
from scipy import spatial

import tensorflow as tf
from keras import optimizers,regularizers
from keras.models import Model,model_from_json
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras import backend as K

from flask import Flask, request, abort, redirect, url_for, jsonify, session, render_template
from flask_session import Session
from flask_cors import CORS

from spider import *

from utils import *

# crawling-artical/review_site
from review_site import * 

# services/ai
from ai import * 


app = Flask(__name__)
CORS(app)

# config
optimizer=optimizers.Adam(lr=0.0005)
MAX_SEQUENCE_LENGTH=128
loss= "categorical_crossentropy"

# ensure responses aren't cached
if app.config["DEBUG"]:
    @app.after_request
    def after_request(response):
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
        response.headers["Expires"] = 0
        response.headers["Pragma"] = "no-cache"
        return response

# configure session to use filesystem (instead of signed cookies)
app.config["SESSION_FILE_DIR"] = gettempdir()
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/get-comments', methods=['GET','POST'])
def crawlComments():
    K.clear_session()

    print(request.method)
    if(request.method == 'POST'):
        # get params
        # sku merchant url in one product
        products = request.get_json().get("products")

        # this is the return list
        comments = []

        for product in products:
            # product object: url, merchant, sku
            spider = Spider(product)
            temp = spider.toJSON()

            if(len(temp) > 0):
                # concat list
                comments = comments+temp

        print("total is: " + str(len(comments)))

        # remove duplicate
        seen_comments = set()
        result_comments = []
        for obj in comments:
            if obj['description'] not in seen_comments:
                result_comments.append(obj)
                seen_comments.add(obj['description'])

        return jsonify(comments = result_comments)
    else:
        return 'Method not allow.'

# this function find similarity between two product name
@app.route('/find-similarity', methods=['GET','POST'])
def findSimilarity():
    K.clear_session()

    if(request.method == 'POST'):
        # get params
        # product = request.args.get('product')
        # productModel = request.args.get('productModel')
        products = request.get_json().get("products")
        productModel = request.get_json().get("productModel")

        if not (products is None or productModel is None):
            # loading
            with open('model/tokenizer.pickle', 'rb') as handle:
                tokenizer = pickle.load(handle)

                sequencesProductModel = tokenizer.texts_to_sequences([productModel])
                sequencesProductModelPadding = pad_sequences(sequencesProductModel, maxlen=MAX_SEQUENCE_LENGTH)

                productsArrResult = list(
                    map(lambda product: productTextToScore(sequencesProductModelPadding, product, tokenizer), products))

                # sorting
                productsArrResult.sort(key=lambda x: x['score'], reverse=True)

                return jsonify(productsArrResult)
        else:
            return 'No cotent.'
    else:
        return 'Method not allow.'

@app.route('/analyze-text', methods=['GET', 'POST'])
def analyzeIndex():
    K.clear_session()

    # { "q": "a" }

    if(request.method == 'POST'):
        # get params
        q = request.get_json().get("q")
        
        print("q " + q)

        attributes = split_sentence_to_array(q)

        tf.global_variables_initializer()

        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())

            # load json and create model
            json_file = open('model/model.json', 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            loaded_model = model_from_json(loaded_model_json)

            # load weights into new model
            loaded_model.load_weights("model/LSTMCNN_ALL_best_weights.hdf5")
            print("Loaded model from disk")

            # evaluate loaded model on test data
            loaded_model.compile(optimizer=optimizer, loss=loss,metrics=['accuracy'])

            # loading
            with open('model/tokenizer.pickle', 'rb') as handle:
                tokenizer = pickle.load(handle)
                data = []

                for x in simpleAnalyzeOntology(q):
                    sequences_test = tokenizer.texts_to_sequences([x[2]])
                    x_test_seq = pad_sequences(sequences_test, maxlen=MAX_SEQUENCE_LENGTH)

                    yhat_cnn = loaded_model.predict(x_test_seq)

                    sentimentResult = get_sentiment(yhat_cnn)

                    temp = {}
                    temp['entity'] = x[0]
                    temp['keywords'] = x[1]
                    temp['sentence'] = x[2]
                    temp['sentiment'] = sentimentResult[0]
                    temp['score'] = str(sentimentResult[1])

                    data.append(temp)

                return jsonify(data = data)
    else:
        abort(400)
        return 'ONLY ACCEPT POST REQUEST'

@app.route('/analyze-text-edit', methods=['GET', 'POST'])
def analyzeIndexEdit():
    K.clear_session()

    # { "q": "a" }

    if(request.method == 'POST'):
        # get params
        q = request.get_json().get("q")
        
        print("q " + q)

        attributes = split_sentence_to_array(q)

        tf.global_variables_initializer()

        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())

            # load json and create model
            json_file = open('model/model.json', 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            loaded_model = model_from_json(loaded_model_json)

            # load weights into new model
            loaded_model.load_weights("model/LSTMCNN_ALL_best_weights.hdf5")
            print("Loaded model from disk")

            # evaluate loaded model on test data
            loaded_model.compile(optimizer=optimizer, loss=loss,metrics=['accuracy'])

            # loading
            with open('model/tokenizer.pickle', 'rb') as handle:
                tokenizer = pickle.load(handle)
                res_data = []
                data = {}

                for x in simpleAnalyzeOntology(q):
                    # print(x)

                    sequences_test = tokenizer.texts_to_sequences([x[2]])
                    x_test_seq = pad_sequences(sequences_test, maxlen=MAX_SEQUENCE_LENGTH)

                    yhat_cnn = loaded_model.predict(x_test_seq)

                    sentimentResult = get_sentiment(yhat_cnn)

                    """
                    x[0]: entity (Ex: "CAMERA")
                    x[1]: [keywords] (Ex: ["mờ", "đẹp"])
                    x[2]: sentence
                    sentimentResult[0] : sentiment
                    sentimentResult[1] : score 
                    """
                    entity = x[0]
                    # Set 'key' and 'value' for average score of an Entities
                    if data.get(entity, 0) == 0:
                        data[entity] = (x[1], [x[2]], sentimentResult[1], 1)
                    else:
                        # Keywords
                        keywords = data[entity][0] + x[1]
                        # Array of sentence
                        sentence = data[entity][1] + [x[2]]
                        # entity_score: total score of a entity
                        entity_score = data[entity][2] + sentimentResult[1]
                        # no_of_appearance: number of appearance of this entity
                        no_of_appearance = data[entity][3] + 1

                        data[entity] = (keywords, sentence, entity_score, no_of_appearance)

                # Get average score for each entity
                for k, v in data.items():
                    temp = {}
                    temp['entity'] = k 
                    temp['keywords'] = list(set(v[0]))
                    temp['sentence'] = list(set(v[1]))
                    
                    average_score = v[2]/ v[3]
                    temp['score'] = average_score
                    if average_score > 0.5:
                        temp['sentiment'] = 'Positive'
                    elif average_score < 0.5:
                        temp['sentiment'] = 'Negative'
                    else:
                        temp['sentiment'] = 'Neutral'
                    
                    res_data.append(temp)

                # print(res_data)
                # print(average_score_entity)

                return jsonify(data = res_data)
    else:
        abort(400)
        return 'ONLY ACCEPT POST REQUEST'

@app.route('/get-article', methods=['GET', 'POST'])
def getContentReviewSite():
    K.clear_session()

    if(request.method == 'POST'):
        # Call object
        tinhte = Tinhte()
        vnreview = Vnreview()

        # Get json 
        url = request.get_json().get("q")
        review = ""
        
        if validateUrl(url) and "tinhte" in url:
            review = tinhte.getArticle(url)
        elif validateUrl(url) and "vnreview" in url:
            review = vnreview.getArticle(url)
            
        return jsonify(review = review)
    else:
        abort(400)
        return 'ONLY ACCEPT POST REQUEST'