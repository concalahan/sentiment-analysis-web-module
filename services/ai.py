import os.path
import json
import numpy as np

from utils import *
from nltk.tokenize import sent_tokenize

my_path = os.path.abspath(os.path.dirname(__file__))

def split_sentence_to_array(sentence):
    results = []

    path = os.path.join(my_path, "../static/attributes.json")

    with open(path, encoding="utf-8") as f:
        attributes = json.load(f)

        for key, values in attributes.items():
            temps = []

            for value in values:
                if(value in sentence):
                    temps.append(value)
            
            if(len(temps) != 0):
                results.append({key: temps})
    
    return results

def get_sentiment(modelResult):
    # modelResult is numpy narray

    # Get the max float in the narray -> what AI classify the sentiment
    maxElement = np.amax(modelResult)

    # Get the indices of maximum element in numpy array
    maxIndex = np.where(modelResult[0] == np.amax(modelResult[0]))

    sentiment = "positive"

    if(maxIndex == 0):
        # negative
        sentiment = "negative"
    elif(maxIndex == 1):
        # neutral
        sentiment = "neutral"

    if(maxElement > 0.6):
        # if AI result is greater than 0.6, then take the result
        return (sentiment, maxElement)
    else:
        # else make it as NEUTRAL
        # print("maxElement " + str(maxElement))
        return ("neutral", 0.5)
    
def separatingParagraph(paragraph):
    path = os.path.join(my_path, "../static/entities.json")

    entities = readJson(path)
    # extend all the entities in json file 
    valuesList = list(item for valueList in entities.values() for item in valueList)
    # Result is a list of each sentence separated by "." and ","
    result = []

    # Split comment using sent_tokenize"."
    # splitComment = paragraph.split(".")
    splitComment = sent_tokenize(paragraph)

    for comment in splitComment:
        # countKey var to check if there are more than 2 values of entities or not
        countKey = 0
        for value in valuesList:
            if value in comment:
                countKey += 1
                if countKey == 2: break
        
        # If there is only one entities in the comment then there is no need to separate the comment
        if countKey == 1:
            result += [comment]
        # else if there is more than 2 entities then we should separate the comment with ","
        elif countKey == 2:
            result += [x for x in comment.split(",")]

    return result

def mergeEntity(comment):
    result = set()

    path = os.path.join(my_path, "../static/entities.json")

    entities = readJson(path)
    
    # Get all the sentence from the paragraph
    sentenceList = separatingParagraph(comment)

    for sentence in sentenceList:
        # FlagExist for the case the we set only the sentence with one entity
        # flagExist = False
        for key in entities:
            for value in entities[key]:
                if value in sentence.lower():
                    # print(key + " - " + value + " - " + sentence)
                    result.add((key,sentence))
                    # flagExist = True
                    break
            # if flagExist is True: break
    
    # Return a tuple: 
    # first is key ("PIN", "MANHINH" , ...)
    # second is the comment sentence

    return result

def mergeAttribute(mergeEntityResult):
    result = list()

    path = os.path.join(my_path, "../static/attributes.json")

    attributes = readJson(path)
    
    for x in mergeEntityResult:
        # x[0] is key ("PIN", "MANHINH" , ...)
        entity = x[0]
        attrList = list()
        for attr in attributes[entity]:
            if attr in x[1].lower() or khongDau(attr) in x[1].lower():
                attrList.append(attr)
        if attrList != []:
            result.append((entity,attrList,x[1]))
                
    return result

def simpleAnalyzeOntology(paragraph):
    # Lower case on the content
    paragraph = paragraph.lower()
    mergeEntityResult = mergeEntity(paragraph)
    result = mergeAttribute(mergeEntityResult)

    return result
