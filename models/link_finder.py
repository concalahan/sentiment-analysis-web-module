from bs4 import BeautifulSoup
from urllib.request import urlopen

import urllib
import json

class LinkFinder():
    def __init__(self):
        self.total_comments = 0
        self.comments = list()

    def page_comments(self):
        return self.comments

    def error(self, message):
        pass

    def fakeUser(self, url):
        headers = {'User-Agent': 'User-Agent:Mozilla/5.0'}
        try:
            dataTemp = urllib.request.Request(url, headers=headers)
            data = urllib.request.urlopen(dataTemp).read()
            soup = BeautifulSoup(data, "lxml")
        except Exception:
            print('Exception : ' + str(Exception.__class__.__name__))

        return soup

    def getProductCommentTiki(self, url):
        soup = self.fakeUser(url)

        h4Result = soup.find("h4", {"name": "results-count"})
        totalProduct = h4Result.get_text().split(" ")[1]

        # 35 is 35 product in a page
        lastPage = (int(totalProduct) / 47) + 1

        # If > 200 => lastPage = 200
        if int(lastPage) > 200:
            lastPage = 200

        for index in range(1, int(lastPage) + 1):
            crawlUrl = url + "?src=mega-menu&page=" + str(index)
            print('Crawling - ' + crawlUrl)
            soup2 = self.fakeUser(crawlUrl)
            try:
                division = soup2.find("div", {"class": "product-box-list"})
                anchors = division.find_all('a')
                for anchor in anchors:
                    href = anchor.get('href')
                    if '?' in href:
                        href = href.split('?')[:-1][0]
                    if '.html' not in href:
                        continue
                    self.links.append(href)
                    self.total_links += 1
            except Exception:
                print("Fail : - " + crawlUrl)
                continue

    def getCommentsShopee(self, productObject):
        if("." in productObject['url']):
            itemId = productObject['sku']
            temp = productObject['url'].split('.')
            
            # because the shopId is 1 space from the split
            shopId = temp[len(temp)-2]

            reviewUrl = """https://shopee.vn/api/v2/item/get_ratings?filter=0&flag=1&itemid=""" + str(itemId) + """&limit=100&offset=0&shopid=""" + str(shopId) + """&type=0"""
            
            try:
                res = urllib.request.urlopen(reviewUrl).read()
                obj = json.loads(res)

                # If list is empty => break the loop
                if (len(obj["data"]['ratings'])) is 0:
                    print("Empty list -> break")

                ratings = obj["data"]['ratings']

                for rating in ratings:
                    # aggregateRatingSchema in models
                    if(rating['comment']):        
                        aggregateRating = {}
                        
                        aggregateRating['subjectOf'] = 'shopee'
                        
                        # we define COMMENT_RATING_TYPE (Eg: identifier) is 1 in main service
                        aggregateRating['identifier'] = 1
                        aggregateRating['ratingValue'] = rating['rating_star']

                        # .strip() remove white space at the start and end
                        aggregateRating['description'] = rating['comment'].strip()

                        # timestamp
                        aggregateRating['dateCreated'] = rating["ctime"]
                        aggregateRating['url'] = productObject['url']

                        # for crawl user
                        # aggregateRating['author']['fullName'] = rating['author_username']
                        # aggregateRating['author']['image'] = 'https://cf.shopee.vn/file/' + rating['author_portrait']

                        # if user post image, save the link
                        # ex: the id is 1234 -> the image is https://cf.shopee.vn/file/1234
                        if(rating['images'] != None):
                            aggregateRating['image'] = list(map(LinkFinder.getImageShopee, rating['images']))

                        self.comments.append(aggregateRating)
                        self.total_comments += 1
            except Exception as e:
                print(e)
        else:
            print("Cannot get shopid for API.")

    def getImageShopee(imageId):
        return 'https://cf.shopee.vn/file/' + imageId   
    
    def getCommentSendo(self, url):
        # url = 'https://www.sendo.vn/m/wap_v2/category/product?category_id=2901&p=1&s=100'
        page = 1

        while True:
            repPage = 'p=' + str(page)
            crawlUrl = url.replace("p=1", repPage)

            print("Crawling : " + str(crawlUrl))

            try: 
                res = urllib.request.urlopen(crawlUrl).read()
                obj = json.loads(res)

                # If list is empty => break the loop
                if (len(obj["result"]['data'])) is 0:
                    print("Empty list -> break")
                    break
                
                data = obj["result"]['data']
                
                for product in data:
                    # If total rate is equal to 0 => Next
                    totalRate = product['rating_info']['total_rated']
                    if totalRate == 0:
                        continue
                    # Get product ID
                    productId = product['id']
                    reviewUrl = "https://www.sendo.vn/m/wap_v2/san-pham/rating/" + str(productId) + "?p=1&s=100"
                    self.links.append(reviewUrl)
                    self.total_links += 1

                # increase number of page
                page += 1
            except Exception:
                print("Fail : - " + crawlUrl)
                page += 1
                continue
        
    # def getCommentVatGia(self, url):


            

