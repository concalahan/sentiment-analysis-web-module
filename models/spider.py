import datetime
import sys
import json

sys.path.append('../')

from utils import *
from link_finder import LinkFinder


now = datetime.datetime.now()

WRITE_DIR = '../data/' + str(now.year) + '/' + str(now.month) + '/' + str(now.day) + '/'

class Spider:
    comments = []

    def __init__(self, productObject):
        # productObject(url, merchant, sku)
        self.gather_comments(productObject)

    @staticmethod
    def gather_comments(productObject):
        Finder = LinkFinder()
        if 'tiki' in productObject['url']:
            Finder.getProductCommentTiki(productObject)
                
        elif 'shopee' in productObject['url']:
            Finder.getCommentsShopee(productObject)
                
        elif 'sendo' in productObject['url']:
            Finder.getCommentSendo(productObject)
                
        # Get all review links
        Spider.comments = Finder.page_comments()

    def toJSON(self):
        return Spider.comments